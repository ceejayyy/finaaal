<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
   
	protected $primaryKey = 'sponsor_serial';
      public function user(){
    	return $this->hasOne('App\User', 'id', 'userid');
    } 
    public function donation(){
    	return $this->hasMany('App\Donation','sponsor_serial','sponsor_serial');
    } 

    
}
