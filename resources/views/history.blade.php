@extends('layouts.historyui')
@section('content')
<?php
use App\Donation;

?>

<head>
    <title>History</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
setInterval(function(){
    $(document).ready(function (){
      var to = 0;
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/total',
            data:{
                '_token': $('input[name=_token]').val()
            },
            success:function(data){
                for(var i=0; i<data.length; i++){
                    to+=data[i].TotalRedeem;
                }
                $('#total').text(to);
                console.log(to);
            },
            error:function(){
            }
        });
    });
},3000);
</script>

<style>
    body {
        background-color: #f3f3f3;
    }
</style>

<div style="background-color: #fff; margin-right: 110px; margin-left: 70px; margin-top: 10px; border-radius: 15px 15px;">
<div class="limiter">
        <div class="container-table100">
            <div class="wrap-table100"><p style="font-size: 15pt; font-family: Lato-Bold; color: black">Current Story</p>
                <div class="table100 ver5 m-b-110">
                    <div class="table100-head">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1">Date</th>
                                    <th class="cell100 column2">Goal</th>
                                    <th class="cell100 column3">Redeem</th>
                                    <th class="cell100 column4">Total</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                    @foreach ($patientDetails as $patients)

                    <div class="table100-body js-pscroll">
                        <form action="{{url('/confirm')}}">
                        <table>
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$patients->created_at->format('d M Y')}}</td>
                                    <td class="cell100 column2">{{$patients->goal}}</td>
                                   
                                    <td class="cell100 column3"><input type="submit" class="btn btn-primary" name="Redeem" value="Redeem" <?php ?> ></td>
                                     <td class="cell100 column4"><div id="total"></td>
                                </tr> 
                                @endforeach                       
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
                


    <div class="wrap-table100"><p style="font-size: 15pt; font-family: Lato-Bold; color: black">Redeemed</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1">Date</th>
                                    <th class="cell100 column2">Goal</th>
                                    <th class="cell100 column3">Amount Redeemed</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                     @foreach ($redeemdetails as $redeem)

                    <div class="table100-body js-pscroll">
                        <table>
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$redeem->created_at->format('d M Y')}}</td>
                                    <td class="cell100 column2">{{$redeem->goal}}</td>
                                    <td class="cell100 column3">{{$redeem->TotalRedeem}}</td>
                                </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

    <div class="wrap-table100"><p style="font-size: 15pt; font-family: Lato-Bold; color: black">History of Donations</p>
        <div class="table100 ver5 m-b-110">
                    <div class="table100-head">
                        <table>
                            <thead>
                                <tr class="row100 head">
                                    <th class="cell100 column1">Date</th>
                                    <th class="cell100 column2">Amount Needed</th>
                                    <th class="cell100 column3">To Whom</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                     @foreach ($sponsorCollect as $sponsors)

                    <div class="table100-body js-pscroll">
                        <table>
                            <tbody>
                                <tr class="row100 body">
                                    <td class="cell100 column1">{{$sponsors->created_at->format('d M Y')}}
                                    <td class="cell100 column2">{{$sponsors->voucherValue}}</td>
                                    @foreach ($sponsors->donation as $sponsors)
                                    @if($sponsors->patient == null)
                                    <td class="cell100 column3"></td>
                                    @else
                                    <td class="cell100 column4">{{$sponsors->patient->userName->name}}</td>
                                    @endif
                                    @endforeach
                                </tr>
                                 @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>

<br>
@endsection
