@extends('layouts.updatepage')

@section('content')

<style>
	body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
</style>

<br><br>
<div style="background-color: #F9F9F9; padding: 65px; padding-top: 30px; max-width: 730px; width: 730px; margin-left: 25%">


@foreach ($ups as $u)

<strong style="letter-spacing: 0.1em; font-size: 13pt; color: black">{{$u->storytitle}}</strong><br>
<span style="letter-spacing: 0.1em; font-size: 11pt; color: #167ED8; font-style: italic;">Date Posted: {{$u->created_at->format('F d, Y')}}</span><br>
@foreach ($u->picture as $update)
<img style="margin-top: 7px;" src="{{  url('storage/picture/'.$update->filename)}}" width="590px" height="380px" /><br>
@endforeach
<div style="background-color: #F2F1F1; height: 200px; width: 600px;">
		<h4 style="margin-top: 5px; color: black; letter-spacing: 0.1em; padding: 15px">{{$u->story}}</h4>
	</div><br><br><br>

  @endforeach

</div> 
<!-- update end -->

	



@endsection
