@extends('layouts.single')


@section('content')

<style>

body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }

	table {
    border-collapse: collapse;
    width: 40%;
}

th, td {
    text-align: left;
    padding: 8px;
    border-radius: 6px 6px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #56B8F9;
    color: white;
    border-radius: 10px 10px;
    text-align: center;
}


</style>

<br><br>
<table align="center">
<tr>
<th>TOTAL OF ALL ANONYMOUS DONATIONS</th>
</tr>

<tr>
	<td>{{$totalanonymous}}</td>
</tr>

</table><br><br><br>

<table align="center">
<tr>
<th colspan="3">TOTAL OF ALL NONANONYMOUS DONATIONS</th>
</tr>
@foreach($nonanonymous as $donor)
	<tr>
	

	<td>{{$donor->sponsor['voucherValue']}}</td>		
	<td>{{$donor->sponsorName}}</td>	
	<td>{{$donor->created_at->format('d M Y')}}</td>	

	
	

	</tr>@endforeach
</table><br><br>

<p style="text-align: center; font-size: 20pt">Total: <strong style="color: #232323">{{$total}}</strong></p>



@endsection

