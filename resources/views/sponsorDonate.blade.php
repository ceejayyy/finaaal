<?php 
use App\Sponsor;
$voucher = Sponsor::where('userid', Auth::id())->where('status', null)->get()->count();

?>

@extends('layouts.raise')
@section('content')


<head>


    <link rel="stylesheet" href="/css/form-basic.css">

</head>

<style>
    body {
        background-image: url('/images/bgg.png');
        top right no-repeat; 
        background-attachment:fixed;
        background-size: cover;
        margin-top: 0px;

    }
</style>

<br><br>

    <div class="main-content">

        <!-- You only need this form and the form-register.css -->

        <div style="float: left;border: 1px solid #e65c00;margin-left: 20px;padding: 15px;color:#ffa366">
          <h3 style="text-align: center"><strong style="font-size: 50px">P{{number_format($patient->TotalRedeem)}}</strong><br> of {{number_format($patient->goal)}} goal</h3>   
        </div>

        <form style="background-color: #f9f9f9;" class="form-basic" action="{{url('/helpxp')}}" method="post">
   
             {{csrf_field()}}

                <input type="hidden" name="patientid" value="{{ $patient->patientid}}">

                    <div class="form-title-row">
@if($voucher == 0)
    <div class="alert alert-info">
        You need to buy voucher/s first.
    </div>
@endif
                        <h1>Donate</h1>
                        <p style="font-style: normal; font-family: Arial Narrow; font-size: 11pt">Make an online donation to help our organization provide treatment, care and support to vulnerable children. Fill out the form below to send your donation. Thank you for your help!</p>
                    </div>

                    <p id="den"></p>
                    Total availed vouchers: <p style="color: blue; font-size: 14pt" id="total"></p>


                    <div class="form-row">
                        <label>
                            <span>Donate to: </span>
                            <span style="font-size: 14pt">{{$patient->patientname}}</span>
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span></span>
                            <span style="font-size: 14pt"><input type="radio" name="anonymous" value="anonymous">&nbsp;Anonymous</span>
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Amount to be donated:</span>
                            <input type="text" name="amount" id="amount">
                        </label>
                    </div>

                   
                    <div class="form-row" style="margin-left: 35px">
                        <button type="submit" name="submit" onclick="check()">Donate</button>
                    </div>

        </form>

    </div>

    <input type="hidden" id="lacking" value="{{$patient['goal'] - $patient['donations']}}">

    <!-- <p id="den">
        <img src="" id="img">
    </p>
    <p id="total"></p> -->

    <script type="text/javascript">
     $(document).ready(function (){
      var to = "";
      var total = "";
      var div=$(this).parent();
        $.ajax({
            type: 'GET',
            url: '/getDen',
            data:{
                '_token': $('p[name=_token]').val()
            },
            success:function(data){
                for(var i=0; i<data.length; i++){
                    to += data[i].value+" = "+data[i].count+" ";
                    
                }   
                total += data[data.length - 1].total;                    
                
               
                $('#den').text(to);
                $('#total').text(total);
                console.log(data);
            },
            error:function(){
            }
        });
    });
</script>

@if(Session::has('info'))
<div class="alert alert-confirm">
    <script>
        confirm('You cannot donate your desired amount. You only have {{ Session::get('avblVoucher', '') }} available vouchers.');
    </script>  
</div>
@elseif(Session::has('alert'))
    <script>
        confirm('You cannot donate your desired amount. Please buy vouchers first.');
    </script>

@elseif(Session::has('success'))
    <script>
        alert('Successful Donation');
    </script>
@elseif(Session::has('message'))
    <script>
        alert('Cant donate with this amount');
    </script>
@elseif(Session::has('notenough'))
    <script>
        alert('You dont have enough vouchers');
    </script>

@endif

<div id="fh5co-feature-product" class="fh5co-section-gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center heading-section">
                        <h3 style="font-size: 23pt">On-going Stories.</h3>
                        <p style="color: #3d3d29">Make an online donation to help our organization provide treatment, care and support to vulnerable people.</p>
                    </div>
                </div>

                
                @foreach ($data as $patients)


<div style="box-sizing: border-box; float: left; width: 24%; padding: 8px; margin-left: 12px;  content: ""; clear: both;
    display: table;">
 <div class="row row-bottom-padded-md" >
  <div class="col-md-12">
    <ul id="fh5co-portfolio-list">

      <li style="background-image: url( {{ url('storage/picture/'.$patients->filename) }}); width: 100%">
                <a href="http://localhost:8000/list/{{$patients['patientid']}}/view" class="color-3">
                  <div class="case-studies-summary">
                    <span>Give Love</span>
                    <h2>{{$patients->userName->name}}</h2><br>
                    <div align="left"><input type="button" name="donate" value="+Read More" class="btn btn-primary-info"></div>
<br>
                    <div class="progress" style="width: 100%;margin-bottom: 0px">
    <div class="progress-bar" role="progressbar" aria-valuenow="{{$patients->TotalRedeem/$patients->goal*100}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$patients->TotalRedeem/$patients->goal*100}}%">
    </div>
  </div>
  <br>
                <p style="text-align:center;font-size: 20px; color: #fff">P{{number_format($patients->TotalRedeem)}} raised of P{{number_format($patients->goal)}}</p>

                  </div>
                </a> 
            </li>

      </ul>
    </div>
  </div>
</div>

@endforeach  


                </div>
            </div>
@endsection

